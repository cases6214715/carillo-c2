const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

//post a product Admin
router.post("/",auth.verify,(req,res)=>{


	const data = {

		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	productController.addProduct(data).then(resultFromController=>res.send(resultFromController))

});

//get all products
router.get("/all",(req,res)=>{

	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});


//get all active
router.get("/active",(req,res)=>{

    productController.getAllActive().then(resultFromController=>res.send(resultFromController));

})


//get product ID
router.get("/:productId",(req,res)=>{

    const productId = req.params.productId;
    productController.getProductById(productId).then(resultFromController=>{
        res.send(resultFromController);
    }).catch(err => {
        res.status(404).send({message: "ProductID does not Exist!"});
    });

});


//update product
router.put("/:productId", auth.verify, (req, res) => {
    const productId = req.params.productId;
    
    const updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    };
    
    productController.updateProductById(productId, updatedProduct).then(resultFromController => {
        res.send(resultFromController);
    
    }).catch(err => {
        res.status(404).send({message: "ProductID does not Exist!"});
    });

});



// Archive a product
router.patch("/:productId/archive", auth.verify, (req, res) => {

    const productId = req.params.productId;

    productController.archiveProduct(productId).then(resultFromController => {
        res.send(resultFromController);

    }).catch(err => {
        res.status(404).send({message: "ProductID does not Exist!"});
    });

});

// Unarchive a product
router.patch("/:productId/unarchive", auth.verify, (req, res) => {

    const productId = req.params.productId;

    productController.unarchiveProduct(productId).then(resultFromController => {
        res.send(resultFromController);

    }).catch(err => {
        res.status(404).send({message:"ProductID does not Exist!"});
    });

});



module.exports = router;