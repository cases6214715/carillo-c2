const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String  
	},

	totalAmount: {
		type: Number,
		default: 0
	},

	totalQuantity: {
		type: Number,
		default: 0
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	products: [
		{

			productId: {
				type: String
			},

			price: {
				type: Number,
				default: 0
			},

			quantity: {
				type: Number,
				default: 0
			}

		}
	],

});


module.exports = mongoose.model("Order", orderSchema);