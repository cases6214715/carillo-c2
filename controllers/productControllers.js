const Product = require('../models/Product');

//add product Admin
module.exports.addProduct = (data) =>{

    if(data.isAdmin){

            let newProduct = new Product({

        name: data.product.name,
        description: data.product.description,
        price: data.product.price

    });

        return newProduct.save().then((product,error)=>{

        if (error){
            return false
        }else{
            return newProduct
        }

    })

    }else{
        return false
    }

};

//get all products
module.exports.getAllProducts = () =>{

    return Product.find({}).then(result=>{
        return result;
    });

};

//get All acitve products
module.exports.getAllActive = () =>{

    return Product.find({isActive: true}).then(result=>{
        return result;
    });

};

//get Product by Id
module.exports.getProductById = (productId) => {

    return Product.findById(productId).then(result=> {
        return  result;
    })
}

  
//update product
module.exports.updateProductById = (productId, updatedProduct) => {
    
    return Product.findByIdAndUpdate(productId, updatedProduct).then(result => {
        
        if(result) {
            
            return "Product updated by Admin";
        
        }else{
            
            return "ProductID does not Exist!";
        }
    });
};


//Archive product
module.exports.archiveProduct = (productId) => {

    return Product.findByIdAndUpdate(productId, { isActive: false }).then(result => {

        if(result) {
            
            return "Product archived successfully";
        
        }else{
            
            return "ProductID does not Exist!";
        }

    });
};

//Unarchive product
module.exports.unarchiveProduct = (productId) => {
    
    return Product.findByIdAndUpdate(productId, { isActive: true }).then(result => {

        if(result) {
            return "Product unarchived successfully";
        }else{
            return "ProductID does not Exist!";
        }

    });
};



