const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");
const bcrypt = require("bcrypt");


//checkEmail
module.exports.checkEmailExists = (reqBody) => {

   
    return User.find({email: reqBody.email}).then(result=>{

        if(result.length>0){
            return ('Email is already registered');
       
        }else{
            return false
        }
    })
};


//register
module.exports.registerUser = (reqBody) =>{

    let newUser = new User({

        firstName : reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)

    })

    return newUser.save().then((user,error)=>{

        if(error){
            return false
        }else{
            return newUser
        }
    })  

}

//login
module.exports.loginUser = (reqBody) =>{

    return User.findOne({email: reqBody.email}).then(result=>{

        if(result == null){

            return false;

        }else{

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){

                return {access: auth.createAccessToken(result)}
            }else{
                return false;
            }

        }


    })
};


//set admin
module.exports.setAsAdmin = (reqParams, reqBody) =>{

    let updatedAdminStatus = {

        isAdmin : true
    }
    return User.findByIdAndUpdate(reqParams.userId, updatedAdminStatus).then((
        user,error)=>{

        if(error){
            return false
        }else{
            return user;
        };

    });

};

//get details
module.exports.getProfile = (data) => {
    
    return User.findById(data.userId).then((result) => {
        
        result.password = "";
        
        return result;
    });
};
