const Order = require("../models/Order");
const Product = require("../models/Product");
const { verify, decode, verifyAdmin } = require("../auth");

// Create new order
module.exports.checkOut = async (userId, reqBody) => {

	const {productId, quantity} = reqBody;

	const product = await Product.findById(productId);
	const totalAmount = product.price * quantity;
	const newOrder = new Order({

		userId: userId,
		products: [
			{
				productId,
				price: product.price,
				quantity  
			}
		],
		totalAmount,
		totalQuantity: quantity
	});

	const updatedProductOrder = await Product.findByIdAndUpdate(productId, {$push:{orders: {productId , quantity }}}, {new: true});

	await updatedProductOrder.save();
	await newOrder.save();

	return newOrder;

}


// Get all orders
module.exports.getAllOrders = () => {

	return Order.find({}).then(result=>{
		return result;
	});

};
